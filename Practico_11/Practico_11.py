# Schwab Urbani, Danel. Visión por computadora: TP11
#
# Considerando los pasos detallados a continuación, realizar una alineación entre imágenes
# como se ejemplifica en las figuras 3 a la 6.
# 1. Capturar dos imágenes con diferentes vistas del mismo objeto
# 2. Computar puntos de interés y descriptores en ambas imágenes
# 3. Establecer matches entre ambos conjuntos de descriptores
# 4. Eliminar matches usando criterio de Lowe
# 5. Computar una homografía entre un conjunto de puntos y el otro
# 6. Aplicar la homografía sobre una de las imágenes y guardarla en otra (mezclarla con
#     un alpha de 50%)

import cv2
import numpy as np

MIN_MATCH_COUNT = 10

img1 = cv2.imread('Img1.jpeg') #Leo primer imagen
img2 = cv2.imread('Img2.jpeg') #Leo segunda imagen

img1_aux = img1.copy()
img2_aux = img2.copy()

dscr = cv2.xfeatures2d.SIFT_create(100) 		#Inicialización del detector y el descriptor

kp1, des1 = dscr.detectAndCompute(img1, None)	#Encontramos los puntos clave y los descriptores con SIFT en img1
kp2, des2 = dscr.detectAndCompute(img2, None)	#Encontramos los puntos clave y los descriptores con SIFT en img2


matcher = cv2.BFMatcher(cv2.NORM_L2)
matches = matcher.knnMatch(des1, des2, k=2)


# Guardamos los buenos matches usando el test de razón de Lowe
good = []
for m,n in matches:
	if m.distance < 0.7*n.distance:
		good.append(m)

if(len(good)>MIN_MATCH_COUNT):
	scr_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
	dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
	
	H,mask = cv2.findHomography(dst_pts, scr_pts, cv2.RANSAC, 5.0) 	#Computamos la homografía con RANSAC

wimg2 = cv2.warpPerspective(img2, H, img2.shape[:2][::-1])			#Aplicamos la transformación perspectiva H a img2

# Puntos clave plot
cv2.drawKeypoints(img1,kp1,img1_aux,(0, 255, 0))
cv2.drawKeypoints(img1,kp2,img2_aux,(0, 0, 255))
img_concatenada = np.concatenate((img1_aux, img2_aux), axis=1)	#Concateno Horizontalmente
cv2.imshow('Puntos clave de las imagenes', img_concatenada)
cv2.imwrite('PuntosClave.jpeg',img_concatenada)
cv2.waitKey(0)

# Post Lowe plot
img_match = cv2.drawMatches(img1, kp1, img2, kp2, good, None)	
cv2.imshow('Post Lowe', img_match)
cv2.imwrite('PostLowe.jpeg',img_match)
cv2.waitKey(0)

#Mezclo imagenes con alpha correspondiente y ploteo
alpha = 0.5
blend = np.array( wimg2*alpha + img1*(1-alpha), dtype=np.uint8)
cv2.imshow('Imagenes mezcladas', blend)
cv2.imwrite('ImagenMezcla.jpeg',blend)
cv2.waitKey(0)
cv2.destroyAllWindows
